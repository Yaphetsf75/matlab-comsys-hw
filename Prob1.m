%%  Signal construction
t1=-1;
t2=1;
fs=2000;
k=1/2;
fc=800;
Ac=10;
t=t1:1/fs:t2;
m1=linspace(0,0,(-t1)*fs);
m2=linspace(2,2,0.1*fs);
m3=linspace(-1,-1,0.1*fs);
m4=linspace(0,0,(t2-0.2)*fs+1);
m=[m1 m2 m3 m4];
figure
plot(t,m);
grid on ; 
ylim([-3 3]);
xlabel('Time');
ylabel('Signal');
%%  Modulation
phi=rand(1);
c=Ac*(1+k*m).*cos(2*pi*fc*t+phi);
figure
plot(t,c);
grid on ;
ylim([0 2.5*Ac]);
ylabel('Modulated signal');
xlabel('Time');
N_FFT=length(t); %N_FFT-point DFT      
X=fftshift(fft(c,N_FFT)); %compute DFT using FFT      
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT; %DFT Sample points   
figure
plot(fs*fVals,abs(X));      
grid on ;
title('Frequency domain');      
xlabel('Frequency')      
ylabel('Foureir');
%%  Coherent
lpFilttt = designfilt('lowpassiir','FilterOrder',8, ...
         'PassbandFrequency',5e1,'PassbandRipple',0.2, ...
         'SampleRate',800);
Filt=filter(lpFilttt,c.*cos(2*pi*fc*t+phi));
figure
plot(t,Filt)
grid on ;
title('Coherent');      
xlabel('Time')      
ylabel('Signal');
%%  envelope detector
figure
plot(t,m,t,envelope(c),t,Filt)
grid on ;
title('Envelope detection');legend ('Original','Envelope','Coherent');      
xlabel('Time')      
ylabel('Signal');
%%  SSB      time domain
X_1=fftshift(fft(m,N_FFT)); %compute DFT using FFT      
fVals_1=(-N_FFT/2:N_FFT/2-1)/N_FFT; %DFT Sample points
X_1=-1i*sign(fVals_1).*X_1;
X_2=ifftshift(X_1);
m_hil=ifft(X_2);
figure
plot(t,abs(m_hil));
grid on ;
title('Hilbert of m in time domain');      
xlabel('Frequency')      
ylabel('Hilbert');
SSB=Ac/2*m.*cos(2*pi*fc*t+phi)-Ac/2*m_hil.*sin(2*pi*fc*t+phi);
figure
plot(t,real(SSB));
grid on ;
title('SSB modulated');      
xlabel('Time')      
ylabel('Modulated Signal');
%%  SSB freq domain
X_3=fftshift(fft(SSB,N_FFT)); %compute DFT using FFT      
fVals_2=(-N_FFT/2:N_FFT/2-1)/N_FFT; %DFT Sample points   
figure
plot(fs*fVals_2,abs(X_3));
grid on ;
title('SSB');      
xlabel('Frequency')      
ylabel('Signal');
%%  SSB demodulation
D_SSB=real(SSB).*cos(2*pi*fc*t+phi);
Filt_1=filter(lpFilttt,D_SSB);
figure
subplot(2,1,1)
plot(t,Filt_1)
grid on ;
title('Coherent');      
xlabel('Time');      
ylabel('Demodulated Signal');
subplot(2,1,2)
plot(t,m)
grid on ;
title('Original');      
xlabel('Time');      
ylabel('Signal');
ylim([-5,5])
    %%end