t=-10:0.01:10;
NFFT=length(t);
m=cos(t);
X_1=fftshift(fft(m,NFFT)); %compute DFT using FFT      
fVals_1=(-NFFT/2:NFFT/2-1)/NFFT; %DFT Sample points
X_1=-1i*sign(fVals_1).*X_1;
X_2=ifftshift(X_1);
m_hil=ifft(X_2);
plot(t,abs(m_hil));