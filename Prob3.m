%%  prob3   DSB Modulation
close all
Ac=10;
fc_1=10e3;
phi_1=0;
fc_2=10e3;
phi_2=5/180*pi;
fm = 1000;
fs = 30000;
t = -4 : 1/fs : 3;
y = 5*sin(2*pi*fm*t);
plot(t,y);
grid on ;
xlabel('Time');
ylabel('m(t)');
xlim([-0.005 0.005])
DSB=y.*cos(2*pi*fc_1*t+phi_1);
figure
plot(t,DSB);
grid on ;
xlabel('Time');
ylabel('Modulated Signal');
N_FFT=length(t); %N_FFT-point DFT
X=fftshift(fft(y,N_FFT)); %compute DFT using FFT
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT; %DFT Sample points
figure
plot(fs*fVals,abs(X));
grid on ;
title('Frequency domain');
xlabel('Frequency')
xlim([-0.5e4,0.5e4])
ylabel('Foureir of Signal');
X_1=fftshift(fft(DSB,N_FFT)); %compute DFT using FFT
figure
plot(fs*fVals,abs(X_1));
grid on ;
title('Frequency domain');
xlabel('Frequency')
ylabel('Foureir of Modulated signal');
xlim([-3e4,3e4])
%% Demodulation   :  Coherent
D_DSB=DSB.*cos(2*pi*fc_2*t+phi_2);
lpFilttt = designfilt('lowpassiir','FilterOrder',6, ...
    'PassbandFrequency',2*fm,'PassbandRipple',0.2, ...
    'SampleRate',fs);
D_DSB_1=filter(lpFilttt,D_DSB);
figure
plot(t,D_DSB_1)
grid on ;
title('Demodulated Signal');
xlabel('Time')
ylabel('m(t)');
xlim([-0.005 0.005])
%%  Costa
z=zeros(1,length(t));
fc_c=fc_2;
j=90;
d=zeros(1,ceil(length(t)/j)+2*j);
for i=j:j:length(t);
    p_1(i-j+1:i)=2*DSB(i-j+1:i).*cos(2*pi*fc_c*t(i-j+1:i)+phi_2);
    p_2(i-j+1:i)=2*DSB(i-j+1:i).*cos(2*pi*fc_c*t(i-j+1:i)+phi_2-pi/2);
    q_1=filter(lpFilttt,p_1);
    q_2=filter(lpFilttt,p_2);
    P_h=(Ac^2*(j)/8)^(-1)*sum((q_1(i-j+1:i)).*(q_2(i-j+1:i)));
    if i/j <= length(d)/2
        fc_c=fc_c+(1.5*P_h*(j/fs));
    else
        fc_c=fc_c-(1.5*P_h*(j/fs));
    end
    z(i-j+1:i)=q_1(i-j+1:i);
    d(i/j)=fc_c;
end
z=wextend('1D','zpd',z,length(t)-length(z),'r');
q_1=wextend('1D','zpd',q_1,length(t)-length(q_1),'r');
q_2=wextend('1D','zpd',q_2,length(t)-length(q_2),'r');
figure
plot(t,z,t,y)
xlim([-0.005 0.005])
ylim([-9 9]);legend ('Costa','Coherent')
grid on ;
title('Costa demodulated');
xlabel('Time')
ylabel('Costa demodulated');
figure
plot(d);
ylim([fc_1-4 fc_1+4])
grid on ;
title('Frequency of VCO');
xlabel('Step')
ylabel('Freq');